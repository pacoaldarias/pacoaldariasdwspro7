<?php
session_start();
if (!$_SESSION['SesionValida']) {
    header("Location: index.php");
}
include_once("funciones.php");
include_once("Modelo.php");
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        cabecera();
        echo "<h2>Profesor Grabar</h2>";

        function leer() {
            $id = recoge("id");
            $nombre = recoge("nombre");
            $profe = new Profesor($id, $nombre);
            return $profe;
        }

        //***************************
        //* Main
        //***************************

        $profesor = leer();

        if ($profesor->getNombre() == "") {
            echo "Error: Nombre vacio" . "<br>";
        } else {
            $modelo = obtenerModelo();
            $res = $modelo->grabarProfesor($profesor);
            if ($res) {
                echo "Grabado: " . $profesor->getNombre() . "<br>";
            } else {
                echo "Error: No grabado: " . $profesor->getNombre() . "<br>";
            }
        }

        inicio();
        pie();
        ?>
    </body>
</html>
