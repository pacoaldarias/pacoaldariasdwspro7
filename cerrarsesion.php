<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        session_start();
        $_SESSION['SesionValida'] = 0;
        session_destroy();
        header("Location: index.php");
        ?>
    </body>
</html>
